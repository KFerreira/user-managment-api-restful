package com.desafio.java.api.usermanagment.ws.signin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.desafio.java.api.usermanagment.model.User;
import com.desafio.java.api.usermanagment.service.UserService;
import com.desafio.java.api.usermanagment.util.Util;
import com.desafio.java.api.usermanagment.util.Validator;
import com.desafio.java.api.usermanagment.ws.ErrorMessage;

/**
 * The Class SignInController.
 *
 * @author keyla.ferreira.mesa
 */
@RestController
public class SignInController {

	@Autowired
	private UserService userService;

	/**
	 * Sign in.
	 *
	 * @param body
	 *            the body
	 * @return the sign in response
	 * @throws MethodArgumentNotValidException
	 */
	@PostMapping(value = "/signIn", headers = "Accept=application/json", produces = "application/json")
	public ResponseEntity<Object> signIn(@RequestBody SignInRequest body)
			throws MethodArgumentNotValidException {
		User newUser = null;
		if (Validator.isValidRequest(body)) {
			newUser = Util.signInRequestToUser(body);
			if (userService.existUserEmail(newUser.getEmail())) {
				return new ResponseEntity<>(new ErrorMessage(
						"The email is already sign in."), HttpStatus.CONFLICT);
			} else {
				userService.signInUser(newUser);
			}

		} else {
			throw new MethodArgumentNotValidException(null, null);
		}
		return new ResponseEntity<>(Util.userToSignInResponse(newUser),
				HttpStatus.CREATED);
	}

	/**
	 * Gets the user service.
	 *
	 * @return the user service
	 */
	public UserService getUserService() {
		return userService;
	}

	/**
	 * Sets the user service.
	 *
	 * @param userService
	 *            the new user service
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
