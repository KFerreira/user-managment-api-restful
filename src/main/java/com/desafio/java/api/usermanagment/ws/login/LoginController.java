package com.desafio.java.api.usermanagment.ws.login;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.desafio.java.api.usermanagment.model.User;
import com.desafio.java.api.usermanagment.service.UserService;
import com.desafio.java.api.usermanagment.util.Util;
import com.desafio.java.api.usermanagment.util.Validator;
import com.desafio.java.api.usermanagment.ws.ErrorMessage;

/**
 * The Class SignInController.
 *
 * @author keyla.ferreira.mesa
 */
@RestController
public class LoginController {

	@Autowired
	private UserService userService;

	/**
	 * Sign in.
	 *
	 * @param body
	 *            the body
	 * @return the sign in response
	 * @throws MethodArgumentNotValidException
	 */
	@PostMapping(value = "/login", headers = "Accept=application/json", produces = "application/json")
	public ResponseEntity<Object> login(@RequestBody LoginRequest body)
			throws MethodArgumentNotValidException {
		User newUser = null;
		if (Validator.isValidRequest(body)) {
			newUser = userService.getUserByMailAndPassword(body.getEmail(),
					DigestUtils.sha256Hex(body.getPassword()));
			if (newUser == null) {
				return new ResponseEntity<>(new ErrorMessage(
						"Invalid User/Password."), HttpStatus.UNAUTHORIZED);
			} else {
				newUser = userService.loginUser(newUser);
			}
		} else {
			throw new MethodArgumentNotValidException(null, null);
		}
		return new ResponseEntity<>(Util.userToLoginResponse(newUser),
				HttpStatus.OK);
	}

	/**
	 * Gets the user service.
	 *
	 * @return the user service
	 */
	public UserService getUserService() {
		return userService;
	}

	/**
	 * Sets the user service.
	 *
	 * @param userService
	 *            the new user service
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
