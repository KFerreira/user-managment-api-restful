package com.desafio.java.api.usermanagment.ws.userprofile;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.desafio.java.api.usermanagment.model.User;
import com.desafio.java.api.usermanagment.service.UserService;
import com.desafio.java.api.usermanagment.util.Util;
import com.desafio.java.api.usermanagment.ws.ErrorMessage;

/**
 * The Class UserController.
 *
 * @author keyla.ferreira.mesa
 */
@RestController
public class UserProfileController {

	@Autowired
	private UserService userService;

	/**
	 * Gets the user profile.
	 *
	 * @param id
	 *            the id
	 * @return the user profile
	 */
	@GetMapping(value = "/userProfile/{id}", headers = "Accept=application/json", produces = "application/json")
	public ResponseEntity<Object> getUserProfile(@PathVariable("id") String id, @RequestHeader(value="authorization") String auth){
		if(StringUtils.isBlank(auth)|| !auth.startsWith("Bearer ")){
			return new ResponseEntity<>(new ErrorMessage(
					"Missing or invalid authorization header."), HttpStatus.UNAUTHORIZED);
		}
		String token = auth.substring(7);
		User user = userService.getUserByIdAndToken(id, token);
		if(user == null){
			return new ResponseEntity<>(new ErrorMessage(
					"Unauthorized."), HttpStatus.UNAUTHORIZED);
		}
		if(!Util.validateSession(user.getLastLogin())){
			return new ResponseEntity<>(new ErrorMessage(
					"Invalid Session."), HttpStatus.UNAUTHORIZED);
		}
			
		return new ResponseEntity<>(Util.userToUserProfileResponse(user),
				HttpStatus.OK);
	}
}
