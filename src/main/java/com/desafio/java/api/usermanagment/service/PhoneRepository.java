package com.desafio.java.api.usermanagment.service;

import org.springframework.data.repository.CrudRepository;

import com.desafio.java.api.usermanagment.model.Phone;

/**
 * The Interface PhoneRepository.
 *
 * @author keyla.ferreira.mesa
 */
public interface PhoneRepository extends CrudRepository<Phone, String> {

}
