package com.desafio.java.api.usermanagment.ws.userprofile;

import java.util.Date;
import java.util.List;

import com.desafio.java.api.usermanagment.model.Phone;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * The Class SignInResponse.
 *
 * @author keyla.ferreira.mesa
 */
public class UserProfileResponse {
	
	private String id;
	private String name;
	private String email;
	private List<Phone> phones;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private Date created;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date modified;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date lastLogin;
    private String token;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}
	public Date getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Phone> getPhones() {
		return phones;
	}
	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}

}
