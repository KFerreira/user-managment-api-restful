package com.desafio.java.api.usermanagment.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desafio.java.api.usermanagment.model.User;
import com.desafio.java.api.usermanagment.util.Util;

/**
 * The Class UserService.
 *
 * @author keyla.ferreira.mesa
 */
@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	/**
	 * Exist user email.
	 *
	 * @param email the email
	 * @return true, if successful
	 */
	public boolean existUserEmail(String email) {
		List<User> users = userRepository.findUserByEmail(email);
		return (users != null && !users.isEmpty());
	}

	/**
	 * Sign in user.
	 *
	 * @param newUser the new user
	 */
	public void signInUser(User newUser) {
		userRepository.save(newUser);
	}

	/**
	 * Gets the user repository.
	 *
	 * @return the user repository
	 */
	public UserRepository getUserRepository() {
		return userRepository;
	}

	/**
	 * Sets the user repository.
	 *
	 * @param userRepository the new user repository
	 */
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	/**
	 * Gets the user by mail and password.
	 *
	 * @param email the email
	 * @param password the password
	 * @return the user by mail and password
	 */
	public User getUserByMailAndPassword(String email, String password) {
		List<User> users = userRepository.findUserByEmailAndPassword(email,
				password);
		if (users != null && !users.isEmpty()) {
			return users.get(0);
		}
		return null;
	}

	/**
	 * Login user.
	 *
	 * @param newUser the new user
	 * @return the user
	 */
	public User loginUser(User newUser) {
		newUser.setLastLogin(new Date());
		newUser.setToken(Util.generateToken(newUser.getId()));
		userRepository.save(newUser);
		return newUser;
	}

	
	/**
	 * Gets the user by id and token.
	 *
	 * @param id the id
	 * @param token the token
	 * @return the user by id and token
	 */
	public User getUserByIdAndToken(String id, String token) {
		List<User> users = userRepository.findByIdAndToken(id, token);
		if (users != null && !users.isEmpty()) {
			return users.get(0);
		}
		return null;
	}

}
