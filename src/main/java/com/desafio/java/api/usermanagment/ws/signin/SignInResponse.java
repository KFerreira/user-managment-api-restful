package com.desafio.java.api.usermanagment.ws.signin;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * The Class SignInResponse.
 *
 * @author keyla.ferreira.mesa
 */
public class SignInResponse {
	
	private String id;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private Date created;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date modified;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date lastLogin;
    private String token;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}
	public Date getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}

}
