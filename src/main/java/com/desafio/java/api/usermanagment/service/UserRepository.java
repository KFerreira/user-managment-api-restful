/**
 * 
 */
package com.desafio.java.api.usermanagment.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.desafio.java.api.usermanagment.model.User;

/**
 * The Interface UserRepository.
 *
 * @author keyla.ferreira.mesa
 */
public interface UserRepository extends CrudRepository<User, String> {
	
	@Query("SELECT u FROM User u WHERE u.email = :email") 
    List<User> findUserByEmail(@Param("email") String email);
	
	@Query("SELECT u FROM User u WHERE u.email = :email AND u.password= :password") 
    List<User> findUserByEmailAndPassword(@Param("email") String email, @Param("password") String password);

	@Query("SELECT u FROM User u WHERE u.id = :id AND u.token= :token")
	List<User> findByIdAndToken(String id, String token);

}
