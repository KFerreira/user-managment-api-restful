/**
 * 
 */
package com.desafio.java.api.usermanagment.util;

import org.apache.commons.lang3.StringUtils;

import com.desafio.java.api.usermanagment.ws.login.LoginRequest;
import com.desafio.java.api.usermanagment.ws.signin.SignInRequest;

/**
 * @author keyla.ferreira.mesa
 *
 */
public class Validator {
	
	/**
	 * Instantiates a new validator.
	 */
	private Validator(){
		super();
	}
	
	/**
	 * Checks if is valid request.
	 *
	 * @param request the request
	 * @return true, if is valid request
	 */
	public static boolean isValidRequest(SignInRequest request){
		if(StringUtils.isBlank(request.getName()) 
				|| StringUtils.isBlank(request.getEmail())
				|| StringUtils.isBlank(request.getPassword())){
			return false;
		}
		return true;
	}
	
	public static boolean isValidRequest(LoginRequest request){
		if(StringUtils.isBlank(request.getEmail())
				|| StringUtils.isBlank(request.getPassword())){
			return false;
		}
		return true;
	}

}
