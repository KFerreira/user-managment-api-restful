/**
 * 
 */
package com.desafio.java.api.usermanagment.util;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.codec.digest.DigestUtils;
import org.joda.time.DateTime;
import org.joda.time.Minutes;

import com.desafio.java.api.usermanagment.model.User;
import com.desafio.java.api.usermanagment.ws.login.LoginResponse;
import com.desafio.java.api.usermanagment.ws.signin.SignInRequest;
import com.desafio.java.api.usermanagment.ws.signin.SignInResponse;
import com.desafio.java.api.usermanagment.ws.userprofile.UserProfileResponse;

/**
 * The Class Util.
 *
 * @author keyla.ferreira.mesa
 */
public class Util {

	/**
	 * Instantiates a new util.
	 */
	private Util() {

	}

	/**
	 * Creates the user from request.
	 *
	 * @param request
	 *            the request
	 * @return the user
	 */
	public static User signInRequestToUser(SignInRequest request) {
		User newUser = new User();
		Date today = new Date();
		try {
			newUser.setId(generateUserId());
			newUser.setName(request.getName());
			newUser.setEmail(request.getEmail());
			newUser.setPassword(DigestUtils.sha256Hex(request.getPassword()));
			newUser.setPhones(request.getPhones());
			newUser.setToken(generateToken(newUser.getId()));
			newUser.setCreated(today);
			newUser.setModified(today);
			newUser.setLastLogin(today);

		} catch (Exception e) {

		}
		return newUser;
	}

	/**
	 * User to sign in response.
	 *
	 * @param newUser
	 *            the new user
	 * @return the sign in response
	 */
	public static SignInResponse userToSignInResponse(User newUser) {
		SignInResponse response = new SignInResponse();
		response.setId(newUser.getId());
		response.setToken(newUser.getToken());
		response.setLastLogin(newUser.getLastLogin());
		response.setCreated(newUser.getCreated());
		response.setModified(newUser.getModified());
		return response;
	}

	/**
	 * User to login response.
	 *
	 * @param newUser
	 *            the new user
	 * @return the login response
	 */
	public static LoginResponse userToLoginResponse(User newUser) {
		LoginResponse response = new LoginResponse();
		response.setId(newUser.getId());
		response.setToken(newUser.getToken());
		response.setLastLogin(newUser.getLastLogin());
		response.setCreated(newUser.getCreated());
		response.setModified(newUser.getModified());
		return response;
	}

	/**
	 * User to user profile response.
	 *
	 * @param user
	 *            the user
	 * @return the user profile response
	 */
	public static UserProfileResponse userToUserProfileResponse(User user) {
		UserProfileResponse response = new UserProfileResponse();
		response.setId(user.getId());
		response.setToken(user.getToken());
		response.setLastLogin(user.getLastLogin());
		response.setCreated(user.getCreated());
		response.setModified(user.getModified());
		response.setName(user.getName());
		response.setEmail(user.getEmail());
		response.setPhones(user.getPhones());
		return response;
	}

	/**
	 * Generate user id.
	 *
	 * @return the string
	 */
	public static String generateUserId() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}

	/**
	 * Generate token.
	 *
	 * @return the string
	 */
	public static String generateToken(String id) {
		String token;
		try {
			token = Jwts
					.builder()
					.setSubject("userProfile")
					.claim("id", id)
					.claim("creationDate", new Date())
					.signWith(SignatureAlgorithm.HS256,
							"secr3t".getBytes("UTF-8")).compact();
		} catch (UnsupportedEncodingException e) {
			UUID uuid = UUID.randomUUID();
			token = uuid.toString();
		}

		return token;
	}

	/**
	 * Validate session.
	 *
	 * @param lastLogin
	 *            the last login
	 * @return true, if successful
	 */
	public static boolean validateSession(Date lastLogin) {
		DateTime startDate = new DateTime(lastLogin);
		DateTime endDate = new DateTime();
		Minutes minutes = Minutes.minutesBetween(startDate, endDate);
		int numberOfMinutes = minutes.getMinutes();
		return numberOfMinutes <= 30;
	}

}
