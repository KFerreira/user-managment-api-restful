package com.desafio.java.api.usermanagment;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserManagmentApplicationTests {

	@Test
	public void contextLoads() {
	}

}
